//
//  ViewController.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit

public class BaseController: UIViewController {

    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    static func showAlert(message : String, alertActions : [UIAlertAction]) -> UIAlertController {
        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alertActions.forEach { (alert) in
            alertController.addAction(alert)
        }
        return alertController
    }

}

