//
//  DashboardViewController.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func goToCalories(_ sender: UIButton) {
        guard let caloriesVC = storyboard?.instantiateViewController(withIdentifier: String(describing: CaloriesViewController.self)) else {return}
        navigationController?.pushViewController(caloriesVC, animated: true)
    }
    @IBAction func goToBMI(_ sender: UIButton) {
        guard let caloriesVC = storyboard?.instantiateViewController(withIdentifier: String(describing: BMICalculatorViewController.self)) else {return}
        navigationController?.pushViewController(caloriesVC, animated: true)
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
