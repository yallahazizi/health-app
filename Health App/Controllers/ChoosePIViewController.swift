//
//  ChoosePIViewController.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit

public protocol UserDataDelegate : class{
    func setPhysicalActivity(piValue : Float)
}
class ChoosePIViewController: UIViewController {
    let pi = ["Sedentary","Low Active","Active","Very Active"]
    var isMale = Bool()
    var piValue = Float()
    weak var userDataDelegate: UserDataDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //Action Method
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
//collectionview delegates and datasource
extension ChoosePIViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pi.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CustomCollectionViewCell.self), for: indexPath) as? CustomCollectionViewCell else {return UICollectionViewCell()}
        cell.imageView.image = UIImage(named: pi[indexPath.row])
        cell.titleLabel.text = pi[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            piValue = isMale ? 1.0 : 1.0
        case 1:
            piValue = isMale ? 1.12 : 1.14
        case 2:
            piValue = isMale ? 1.27 : 1.27
        case 3:
            piValue = isMale ? 1.54 : 1.54
        default :
            piValue = 0
        }
        if piValue.isZero == false{
            self.userDataDelegate?.setPhysicalActivity(piValue: Float(piValue))
        }
        self.dismiss(animated: true, completion: nil)
    }

    
}
//Custom collectionviewcell class
class CustomCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
