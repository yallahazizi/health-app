//
//  LoginViewController.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
    }
    

  

    //Mark:- signin button action method
    @IBAction func signIn(_ sender: UIButton) {
        let signInVC = Constants.storyBoard.instantiateViewController(withIdentifier: String(describing: DashboardViewController.self))
        navigationController?.pushViewController(signInVC, animated: true)
    }
    @IBAction func signUp(_ sender: UIButton) {
    let signUpVC = Constants.storyBoard.instantiateViewController(withIdentifier: String(describing: SignUpViewController.self))
        self.present(signUpVC, animated: true, completion: nil)
        
    }
}

    //Mark:- textfield delegate
extension LoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldEmail {
            return textFieldPassword.becomeFirstResponder()
        }
        else if textField == textFieldPassword {
            return textFieldPassword.resignFirstResponder()
        }
        return true
    }
}
