//
//  BMICalculatorViewController.swift
//  Health App
//
//  Created by Laala on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit
import Foundation

class BMICalculatorViewController: UIViewController {
    //Outlets
    @IBOutlet weak var calculatedBMI: UILabel!
    @IBOutlet weak var textFieldHeight: UITextField!
    @IBOutlet weak var textFieldWeight: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    //Action methods
    @IBAction func calculateBMI(_ sender: UIButton) {
        guard let weight : Float = Float(self.textFieldWeight.text!), let height : Float = Float(self.textFieldHeight.text!) else {
            let alert = BaseController.showAlert(message: "Please fill correct information", alertActions: [Constants.tryAgain])
            self.present(alert, animated: true, completion: nil)
            return}
        let bmi = Constants.calculateBMI(weight: weight, height: height)
        self.calculatedBMI.text = String(bmi)
    }
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
