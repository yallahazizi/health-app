//
//  CaloriesViewController.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit
enum piIntensityState : Float {
    case sedentary
    case lowActive
    case active
    case veryActive
}
class CaloriesViewController: UIViewController {
    var piIntensityState : piIntensityState?
    var isMale = true
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var textFieldAge: UITextField!
    @IBOutlet weak var textFieldHeight: UITextField!
    @IBOutlet weak var textFieldWeight: UITextField!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblPIValue: UILabel!
    let choosePIVC = ChoosePIViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedControl.selectedSegmentIndex = 0

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        choosePIVC.userDataDelegate = self
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ChoosePIViewController {
            destination.userDataDelegate = self
        }
    }

    @IBAction func choosePI(_ sender: UIButton) {
        guard let piVC = storyboard?.instantiateViewController(withIdentifier: String(describing: ChoosePIViewController.self)) as? ChoosePIViewController else {return}
        piVC.isMale = self.isMale
        piVC.userDataDelegate = self
        self.present(piVC, animated: true, completion: nil)
    }
    //Action Methods
    @IBAction func calculateCalories(_ sender: UIButton) {
        guard let age : Float = Float(textFieldAge.text!), let height : Float = Float(textFieldHeight.text!), let weight : Float = Float(textFieldWeight.text!) else {
            let alert = BaseController.showAlert(message: "Please fill correct information", alertActions: [Constants.tryAgain])
            self.present(alert, animated: true, completion: nil)
            return}
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            let caloriesForMale = Constants.calculateMaleCalories(age: age, pi: 0 , weight: weight, height: height)
            self.lblCalories.text = String(caloriesForMale)
        case 1:
            let caloriesForFemale = Constants.calculateFemaleCalories(age: age, pi: 0 , weight: weight, height: height)
            self.lblCalories.text = String(caloriesForFemale)
        default:
            break
        }
        self.view.endEditing(true)
    }
    @IBAction func chooseSegment(_ sender: UISegmentedControl) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            isMale = true
        case 1:
            isMale = false
        default:
            isMale = true
        }
    }
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
//Custom Delegates
extension CaloriesViewController : UserDataDelegate{
    func setPhysicalActivity(piValue: Float){
            self.lblPIValue.text = String(piValue)
    }
    
    
}

