//
//  SignUpViewController.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    //Outlets
    @IBOutlet weak var textFieldName: UITextField!
    @IBOutlet weak var textFieldIdentifier: UITextField!
    @IBOutlet weak var textFieldAge: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    //Action Method
    @IBAction func dismiss(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
