//
//  Constants.swift
//  Health App
//
//  Created by Usama Khan on 06/08/2019.
//  Copyright © 2019 Usama Khan. All rights reserved.
//

import Foundation
import UIKit
public class Constants {
    public static let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
    public static let tryAgain = UIAlertAction(title: "Try Again", style: .destructive, handler: nil)
    public static func calculateFemaleCalories(age : Float, pi : Float, weight : Float,height : Float) -> Float {
        let netWeight : Float = 10.9 * weight
        let netHeight : Float = 660.7 * height
        let firstValue : Float = (387 - 7.31) * age
        let secondValue : Float = firstValue + pi
        let thirdValue : Float = netWeight + netHeight
        let calories = secondValue * thirdValue
        return calories
    }
    public static func calculateMaleCalories(age : Float, pi : Float, weight : Float,height : Float) -> Float {
        let netWeight : Float = 14.2 * weight
        let netHeight : Float = 503 * height
        let firstValue : Float = (864 - 9.72) * age
        let secondValue : Float = firstValue + pi
        let thirdValue : Float = netWeight + netHeight
        let calories = secondValue * thirdValue
        return calories
    }
    public static func calculateBMI(weight : Float, height: Float) -> Float {
        let firstValue = (weight / height)
        let bmi = firstValue * firstValue
        return bmi
    }
}
